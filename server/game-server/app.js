var pomelo = require('pomelo');
var bearcat = require('bearcat');
var kakalog = require('pomelo-logger').getLogger('kaka');

//var DBServer = require('./app/components/dbserver');

/**
 * Init app for client.
 */
var app = pomelo.createApp();
app.set('name', 'TestPomelo');

// start app
var contextPath = require.resolve('./context.json');
bearcat.createApp([contextPath], {
  'BEARCAT_LOGGER': 'off'
});


global.bearcat = bearcat;
bearcat.start(function () {
  kakalog.info('bearcat started');

  app.set('bearcat', bearcat);

  // --------- test code --------
  //Test();
  //return;
  // --------- test code over --------    

  configServers();
  

  kakalog.info('app.start');
  // start app
  app.start();
});



function configServers() {
	// app configuration
	app.configure('production|development', 'connector', function(){
	  app.set('connectorConfig',
	    {
	      connector : pomelo.connectors.hybridconnector,
	      heartbeat : 3,
	      useDict : true,
	      useProtobuf : true
	    });

	  	///E:\TestPomelo\game-server\app\servers\connector\filter
		var testFilter = require('./app/servers/connector/filter/testFilter');
	  	app.filter(testFilter());

		var helloWorld = require('./app/components/HelloWorld');
	  	app.load(helloWorld, {interval: 15000});

	});

	app.configure('production|development', 'master', function() {
		var helloWorld = require('./app/components/HelloWorld');
	  	app.load(helloWorld, {interval: 5000});
	});

  	app.configure('production|development', 'db', function () {
  		var DBServer = require('./app/components/dbserver');
    	app.load(DBServer);
  	});

}




// start app
//app.start();

process.on('uncaughtException', function (err) {
  console.error(' Caught exception: ' + err.stack);
});
