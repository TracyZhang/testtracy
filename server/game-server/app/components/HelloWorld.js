// components/HelloWorld.js
module.exports = function(app, opts) {
  return new HelloWorld(app, opts);
};

var DEFAULT_INTERVAL = 3000;

var HelloWorld = function(app, opts) {
  this.app = app;
  this.interval = opts.interval || DEFAULT_INTERVAL;
  this.timerId = null;
};

HelloWorld.name = '__HelloWorld__';

HelloWorld.prototype.start = function(cb) {
  console.log('Hello World Start');
  var self = this;
  this.timerId = setInterval(function() {
      //console.log(self.app.getServerId() + ": Hello World!");
      var memoryUsage = process.memoryUsage();
      //console.log("info = ",memoryUsage);    
      const startUsage = process.cpuUsage();
      //console.log("cpuUsage = ",startUsage);   
      var os = require('os');
      var loads = os.loadavg();
      //console.log("loads = ",loads);   

      // var _ = require('lodash');
      // var ps = require('current-processes');
      // ps.get(function(err, processes) {
      //   var sorted = _.sortBy(processes, 'cpu');
      //   var top5  = sorted.reverse().splice(0, 5);
      //   console.log(top5);
      // });
      
      //var j = 0;
      //for(var i = 1 ; i <1000000000; i++){
      //    j = j+i*2+2;
      //    var d = new Date();
      //    j = j + d.getTime();
      ///} 
      //var klog = require('pomelo-logger').getLogger('kaka');
      //klog.info('Test j = ',j);

    }, this.interval);
  process.nextTick(cb);
}

HelloWorld.prototype.afterStart = function (cb) {
  console.log('Hello World afterStart');
  process.nextTick(cb);
}

HelloWorld.prototype.stop = function(force, cb) {
  console.log('Hello World stop');
  clearInterval(this.timerId);
  process.nextTick(cb);
}