var util = require('util');
var logger = require('pomelo-logger').getLogger('2048', 'Utils');
var crc = require('crc');
var crypto = require('crypto');
var bearcat = require('bearcat');
var os = require('os');

var Utils = function () {
    this.consts = null;
}

var handler = Utils.prototype;

// callback util
handler.invokeCallback = function (cb) {
    if (!!cb && typeof cb == 'function') {
        cb.apply(null, Array.prototype.slice.call(arguments, 1));
    }
};

//generate a random number between min and max
//[min,max]
handler.rand = function (min, max) {
    var v = Math.floor((min + (max - min + 1) * Math.random()));
    if (v > max)
        v = max;
    return v;
};

// clone a object
handler.clone = function (o) {
    var n = {};
    for (var k in o) {
        n[k] = o[k];
    }

    return n;
};

handler.now = function () {
    return new Date().getTime() / 1000;
}

handler.getDay = function () {
    return new Data().getDay();
}

handler.getServerNum = function (app, server) {
    var servers = app.getServersByType(server);
    if (!servers || servers.length === 0) {
        return 0;
    }
    return servers.length;
}

handler.makeLogicServerId = function (logicSId) {
    return this.consts.LogicServerPre + logicSId;
}

handler.getIdFormServerId = function (serverId) {
    var subs = serverId.split('-');
    if (subs.length == 3) {
        return parseInt(subs[2]);
    }
    return -1;
}

handler.crc = function (key) {
    return Math.abs(crc.crc32(key));
}

handler.getRandServerId = function (app, server) {
    var num = this.getServerNum(app, server);
    if (num == 0)
        return 1;
    return this.rand(0, num - 1) + 1;
}

handler.getServerIdByIndex = function (app, server, index) {
    var num = this.getServerNum(app, server);
    if (num == 0)
        return 1;
    return index % num + 1;
}


handler.generateToken = function () {
    return this.rand(0, 10000);
}

var s_agents = ['Android', 'iPhone', 'Windows NT'];

handler.getPlatformFromAgent = function (agent) {
    for (var i = 0; i < s_agents.length; i++) {
        if (agent.indexOf(s_agents[i]) > 0)
            return s_agents[i];
    }
    return 'any';
}

handler.bindDEvent = function (events, bind, event, self, handler) {
    if(!events)
        return;
    if (bind)
        events.on(event, self, handler);
    else
        events.off(event, self, handler);
}


handler.makeLoginUrl = function () {
    var config = bearcat.getBean('globalConfig').getExtConfig();

    var loginUrl = config.jump8loginurl;
    loginUrl += "?Merid=MERID&Timestamp=TIMES&Rcode=RCODE&Sign=SIGN";


    var merId = config.jump8merid;
    var appSecret = config.jump8appsecret;
    var timeStamp = Math.floor(new Date().getTime() / 1000).toString();
    var Rcode = 'HMoba' + Math.floor(Math.random() * 100);

    var str = merId + timeStamp + Rcode + appSecret;
    var md5 = crypto.createHash('md5');
    var sign = md5.update(str).digest('hex');

    loginUrl = loginUrl.replace('MERID', merId);
    loginUrl = loginUrl.replace('TIMES', timeStamp);
    loginUrl = loginUrl.replace('RCODE', Rcode);
    loginUrl = loginUrl.replace('SIGN', sign);

    console.log("loginUrl:", loginUrl);
    return loginUrl;
}

handler.makeMD5 = function (str) {
    var md5 = crypto.createHash('md5');
    return md5.update(str).digest('hex');
}

handler.safeParseInt = function (v) {
    var ret = parseInt(v);
    //if (ret == NaN)
    if(isNaN(ret))
        ret = 0;
    return ret;
}

handler.safeJsonParse = function (v) {
    return this.trySafeJsonParse(v, true);
}

handler.trySafeJsonParse = function (v, showErr) {
    try {
        return JSON.parse(v);
    }
    catch (e) {
        if (showErr) {
            logger.error('bad json:', v);
            logger.error(e);
        }
        return null;
    }
}

handler.copyArray = function (dest, doffset, src, soffset, length) {
    // logger.info('args:', dest, doffset, src, soffset, length);
    // logger.info('dest:', dest);
    // logger.info('doffset:', doffset);
    // logger.info('src:', src);
    // logger.info('soffset:', soffset);
    // logger.info('length:', length);
    if ('function' === typeof src.copy) {
        // Buffer
        src.copy(dest, doffset, soffset, soffset + length);
    } else {
        // Uint8Array
        for (var index = 0; index < length; index++) {
            dest[doffset++] = src[soffset++];
        }
    }
};

handler.getRC4Key = function () {
    return 'www.jumpw.com';
}

// str
// return buffer
handler.rc4EncodeStr = function (key, str) {
    var result = null;
    var ciper = crypto.createCipheriv('rc4', key, '');
    result = ciper.update(str, 'utf8');

    var f = ciper.final();
    return Buffer.concat([result, f], result.length + f.length);
}

handler.strToUint8Arr =function(str){
    var arr =[];
    for(var i= 0; i<str.length; i++){
        arr.push(str.charCodeAt(i));
    }
    var tmpUint8Arr = new Uint8Array(arr);
    return tmpUint8Arr;
}


// decode
handler.rc4Decode = function (key, buf) {
    var result = null;
    var ret = null;
    var decipher = crypto.createDecipheriv('rc4', key, '');
    // result = decipher.update(buf, 'binary', 'utf8');
    
    result = decipher.update(buf, 'binary');
    ret = new Uint8Array(result);
    var rem = decipher.final('binary') ;
    if(rem.length > 0){
        var tmprem = this.strToUint8Arr(rem);
        var tmpret = new Uint8Array(new ArrayBuffer(ret.length + tmprem.length));
        tmpret.set(ret);
        tmpret.set(tmprem, ret.length);
        return tmpret;
    }
    // logger.info('result:', ""+result+ "     json="+ JSON.stringify(result) );
    return ret;
}

handler.jumpRC4 = function (buf) {
    var rc4 = bearcat.getBean('jumpRC4');

    var key = new Buffer('www.jumpw.com');
    var result = new Buffer(buf.length);
    buf.copy(result, 0, 0, buf.length);
    rc4.RC4(key, result);

    return result;
}

handler.getPromoterIdPrefix = function (channel) {
    if (channel == 'jump8')
        return 'j';
    return 'd';
}

handler.getChannelName = function (prefix) {
    if (prefix == 'j')
        return 'jump8';
    return 'default';
}

handler.makeDefaultName = function(uid) {
    // 根据uid
    var rand = this.rand(1,100);
    return 'player' + rand + uid;
}


handler.getNetMac = function () {
    var network = os.networkInterfaces();
    var netkey = Object.keys(network);
    for (var i = 0; i < netkey.length; i++) {
        var netitem = network[netkey[i]];
        if (Array.isArray(netitem)) {
            for (var j = 0; j < netitem.length; j++) {
                var fam = netitem[j].family.toLowerCase();
                if (fam == 'ipv4' && netitem[j].mac != '00:00:00:00:00:00') {
                    return netitem[j].mac;
                }
            }
        }else{
            if(netitem.family != null ){
                var fam = netitem.family.toLowerCase();
                if (fam == 'ipv4' && netitem.mac != '00:00:00:00:00:00') {
                    return netitem.mac;
                }
            }
        }
    }
}

handler.getLocalIp = function () {
    var network = os.networkInterfaces();
    var netkey = Object.keys(network);
    var reIPCheck = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/;
    for (var i = 0; i < netkey.length; i++) {
        var netitem = network[netkey[i]];
        if (Array.isArray(netitem)) {
            for (var j = 0; j < netitem.length; j++) {
                var fam = netitem[j].family.toLowerCase();
                var ipaddr = netitem[j].address;
                if(fam == 'ipv4' && reIPCheck.test( ipaddr) && ipaddr != '127.0.0.1'){
                    return ipaddr;
                }
            }
        }else{
            if(netitem.family != null ){
                var fam = netitem.family.toLowerCase();
                var ipaddr = netitem.address;
                if(fam == 'ipv4' && reIPCheck.test( ipaddr) && ipaddr != '127.0.0.1'){
                    return netitem.mac;
                }
            }
        }
    }
}


handler.ip2int = function (ip) {
    var num = 0;
    ip = ip.split(".");
    num = Number(ip[0])*256*256*256 + Number(ip[1])*256*256+Number(ip[2])*256+Number(ip[3]);
    // num = num >>>0;
    return num;
}

handler.ip2word = function (ip) {
    ip = ip.split(".");
    var word1 = Number(ip[2])*256+Number(ip[3]);
    var word2 = Number(ip[0])*256+Number(ip[1]);
    // num = num >>>0;
    return (word1 ^ word2);
}

handler.randMac = function(){
    var ret = '';
    for(var i = 0; i< 6; i++){
        var rd = Math.floor(255*Math.random());
        if(ret.length >0){
            ret += ':'+rd.toString(16);
        }else{
            ret += rd.toString(16);
        }
    }
    return ret;
}


handler.strTrim = function(s){
    return s.replace(/^\s+|\s+$/gm, '');
}

handler.strEndWith = function(str, withStr){
    if(str == null||str==""||withStr==""||withStr==null){
        return false;
    }

    if(str.length < withStr.length){
        return false;
    }

    if(str.substring(str.length - withStr.length) == withStr){
        return true;
    }

    return false;
}



handler.strStartWith = function(str, withStr){
    if(str == null||str==""||withStr==""||withStr==null){
        return false;
    }

    if(str.length < withStr.length){
        return false;
    }

    if(str.substring(0, withStr.length) == withStr){
        return true;
    }

    return false;
}



// process.pid

module.exports = {
    id: "utils",
    func: Utils,
    props: [{
        name: 'consts',
        ref: 'consts'
    }]
}