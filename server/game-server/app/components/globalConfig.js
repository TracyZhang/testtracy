var logger = require('pomelo-logger').getLogger('2048', '');
var rf = require('fs');
var pomelo = require('pomelo');
var readline = require('readline');
var bearcat = require('bearcat');

var GlobalConfig = function () {
    this.config = {};
    this.extConfig = {};
    this.redisAndCenterConfig = {};
    this.app = pomelo.app;
};

var ptype = GlobalConfig.prototype;

ptype.init = function () {
    var self = this;
    var data = rf.readFileSync('./config/globalconfig.json', 'utf-8');
    this.config = JSON.parse(data);
    logger.info('config loaded:', this.config);

    var data = rf.readFileSync('./config/serverext.json', 'utf-8');
    this.extConfig = JSON.parse(data);
    logger.info('ext loaded:', this.extConfig);

    this.redisAndCenterConfig = this.extConfig.redisandcenter;
    logger.info('redisAndCenterConfig loaded:', this.redisAndCenterConfig);



}

ptype.getConfig = function () {
    return this.config;
}

ptype.getExtConfig = function () {
    return this.extConfig;
}


ptype.getRedisAndCenterConfig = function () {
    return this.redisAndCenterConfig;
}

module.exports = {
    id: "globalConfig",
    func: GlobalConfig,
    init: "init"
}