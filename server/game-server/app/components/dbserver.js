var async = require('async');

// components/dbserver.js
// db服务器启动关闭处理
module.exports = function (app, opts) {
    return new DBServer(app, opts);
};

// 一些服务器相关的初始化,关闭处理
var DBServer = function (app, opts) {
    this.app = app;
    this.opts = opts;
};

DBServer.name = '__db__';
var ptype = DBServer.prototype;

ptype.start = function (cb) {
    process.nextTick(cb);
}

ptype.afterStart = function (cb) {
    var self = this;
    bearcat.getBean('platformMgr').start();
}

// TODO:关闭时,如果保证存档完整
ptype.stop = function (force, cb) {
}