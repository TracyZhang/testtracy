var async = require('async');
var pomelo = require('pomelo');
var klog = require('pomelo-logger').getLogger('kaka');

module.exports = function(app) {
  return new Handler(app);
};

var Handler = function(app) {
  this.app = app;
};

/**
 * New client entry.
 *
 * @param  {Object}   msg     request message
 * @param  {Object}   session current session object
 * @param  {Function} next    next step callback
 * @return {Void}
 */
Handler.prototype.entry = function(msg, session, next) {
  klog.info('xyzYYYYY');
  next(null, {code: 200, msg: 'game server is ok.'});
};


//MsgC2SReqLogin
Handler.prototype.reqLogin = function(msg, session, next) {
	var klog = require('pomelo-logger').getLogger('kaka');
  klog.info('reqLogin');
  var MsgCode = bearcat.getBean('msgCode');
  // 创建角色
  //logger.info('entry:', msg);
  var ip = session.__session__.__socket__.remoteAddress.ip;

  var self = this;
  var taskTryDbAuth = this.tryDbAuth(self,msg,session);

    //处理db auth 的结果 DBAuthRet
  var taskOnRetDbAuth =  function(dBAuthRet, callback) {  //dBAuthRet
      if(dBAuthRet.code != MsgCode.Succ) {
            next(null, {
                code: dBAuthRet.code
            }); 
            callback(true);
            return;         
        }
        var uid = dBAuthRet.uid;
        var sessionService = self.app.get('sessionService');
        // duplicate log in
        // 顶掉原来的号
        if( !!sessionService.getByUid(uid)) {
            next(null, {
                code: MsgCode.OnlineAlready
            });

            var oldSessions = sessionService.getByUid(uid);   
            //logger.info('oldSessions:', oldSessions);
            if(oldSessions.length > 0) {                
                sessionService.kickBySessionId(oldSessions[0].id, 
                    0, null);
            }                

            callback(true);
            return;
        }

        callback(null, dBAuthRet);           
    }

    var refreshSession = function(dBAuthRet, callback) {
        var uid = dBAuthRet.uid;
        var sessionService = self.app.get('sessionService');            
        session.bind(uid);
        session.set('ip', ip);
        session.pushAll(function(err) {
            if(err) {
                console.error('set rid for session service failed! error is : %j', err.stack);
            }
        });
        session.on('closed', onUserLeave.bind(null, self.app)); 
        var loginMsgFactory = bearcat.getBean('loginMsgFactory');
        var msgS2CAckLogin = new loginMsgFactory.MsgS2CAckLogin();
        msgS2CAckLogin.code = MsgCode.Succ;
        msgS2CAckLogin.uid = uid;
        next(null, msgS2CAckLogin);
        callback(null,uid);
    }

    var ntfLogicPlayerEnter = function(uid,callback){
      var dBMsgDefine = bearcat.getBean('dBMsgDefine');
      var msgPlayerEnter = new dBMsgDefine.MsgPlayerEnter();
      msgPlayerEnter.uid = uid;
      msgPlayerEnter.frontendId = self.app.get('serverId');;
      self.app.rpc.logic.logicRemote.enterLogic(null, msgPlayerEnter, function (rpcRet) {
        klog.info("logicRemote.enter ret:", rpcRet);
        callback(null);
      });
    }

    async.waterfall([taskTryDbAuth,taskOnRetDbAuth,refreshSession,ntfLogicPlayerEnter],
        function(err) {});  


};

//  申请db认证
//  MsgC2SReqLogin
Handler.prototype.tryDbAuth = function(self,msg,session){
    return function(callback) {
        var dBMsgDefine = bearcat.getBean('dBMsgDefine');
        var dBAuthInfo = new dBMsgDefine.DBAuthInfo();
        dBAuthInfo.channel = msg.channel;
        dBAuthInfo.user = msg.username;
        dBAuthInfo.pass = '';
        //kakassilog.info('tryDbAuth dBAuthInfo:', dBAuthInfo)
        self.app.rpc.db.dbRemote.auth(session, dBAuthInfo, function(dBAuthRet) {//dBAuthRet
            //kakassilog.info('reqLogin:tryDbAuth :db.auth ret:', dBAuthRet);
            callback(null, dBAuthRet);
        });
    }
} 



/**
 * onUserLeave
 * socket断开 玩家掉线的处理
 * @param {Object} app current application
 * @param {Object} session current session object
 *
 */
var onUserLeave = function(app, session) {
    if(!session || !session.uid) {
      return;
    } 

    //klog.info('onUserLeave:', session.uid)
    //klog.info("onUserLeave(connector entryHandler):uid="+session.uid); 
    //var args = new Object();
    //args.uid = session.uid;
    //app.rpc.logic.offlineRemote.clientOffline(session, args, function() {});     
    var args = new Object();
    args.uid = session.uid;
    //app.rpc.logic.reqOffline(session, args, function() {        
    //});  
    app.rpc.logic.logicRemote.reqOffline(null, args, function () { });

};




/**
 * Publish route for mqtt connector.
 *
 * @param  {Object}   msg     request message
 * @param  {Object}   session current session object
 * @param  {Function} next    next step callback
 * @return {Void}
 */
Handler.prototype.publish = function(msg, session, next) {
	var result = {
		topic: 'publish',
		payload: JSON.stringify({code: 200, msg: 'publish message is ok.'})
	};
  next(null, result);
};

/**
 * Subscribe route for mqtt connector.
 *
 * @param  {Object}   msg     request message
 * @param  {Object}   session current session object
 * @param  {Function} next    next step callback
 * @return {Void}
 */
Handler.prototype.subscribe = function(msg, session, next) {
	var result = {
		topic: 'subscribe',
		payload: JSON.stringify({code: 200, msg: 'subscribe message is ok.'})
	};
  next(null, result);
};
