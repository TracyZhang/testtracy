var async = require('async');
var bearcat = require('bearcat');

var logger = require('pomelo-logger').getLogger('2048', __filename, process.pid);
var kakassilog = require('pomelo-logger').getLogger('kakassidebug');
var bearcat = require('bearcat');

/*
 * DB
*/
var DBRemote = function (app) {
	//logger.info('DBRemote');
	this.app = app;
	this.authMgr = bearcat.getBean('authMgr');
	this.redisDBClient = bearcat.getBean('redisDBClient');
	this.utils = bearcat.getBean('utils');
};

var ptype = DBRemote.prototype;

/**
 * DBRemote auth.
 *
 * @param  {DBAuthInfo}   args (channel\ user\ pass\params  \ userSource)
 * @param  {Function}   cb
 * @return {Void}
 */
ptype.auth = function (dBAuthInfo, cb) {
	var self = this;

	self.authMgr.auth(dBAuthInfo, function (dBAuthRet) {//DBAuthRet
        kakassilog.info('DBRemote:db.auth ret:', dBAuthRet);
		self.utils.invokeCallback(cb, dBAuthRet);
	});
}

ptype.loadPlayer = function (args, cb) {
	var self = this;
	self.redisDBClient.loadPlayer(args, function (ret) {
		self.utils.invokeCallback(cb, ret);
	});
}

ptype.savePlayer = function (args, cb) {
	var self = this;
	self.redisDBClient.savePlayer(args, function (ret) {
		self.utils.invokeCallback(cb, ret);
	});
}

ptype.incPlayerNum = function (args, cb) {
	this.authMgr.incPlayerNum();
	this.utils.invokeCallback(cb);
}

ptype.decPlayerNum = function (args, cb) {
	this.authMgr.decPlayerNum();
	this.utils.invokeCallback(cb);
}

ptype.getPlayerNum = function (args, cb) {
	var ret = this.authMgr.getPlayerNum();
	this.utils.invokeCallback(cb, ret);
}



module.exports = function (app) {
  return new DBRemote(app);
};