var async = require('async');
var logger = require('pomelo-logger').getLogger('2048', 'AuthMgr');
var playernumlogger = require('pomelo-logger').getLogger('playernum');
var pomelo = require('pomelo');
var kakassilog = require('pomelo-logger').getLogger('kakassidebug');

var AuthMgr = function() {	
	this.app = pomelo.app;
	this.authHandlers = {}	
	this.playerNum = 0;
	this.maxPlayerNum = 4999;

	logger.info('AuthMgr');
};

var service = AuthMgr.prototype;

service.init = function() {
	this.utils = bearcat.getBean('utils');
	this.platformMgr = bearcat.getBean('platformMgr');
	this.config = bearcat.getBean('globalConfig').getConfig();
	this.maxPlayerNum = this.config.maxplayers;
}

// .channel
// .user
// .pass			
// 
// cb return
// 		.code
// 		.uid
// 		.channel
// 		.accountId
// 		.errstr
// 		
/**
 * AuthMgr auth.
 *
 * @param  {DBAuthInfo}   args (channel\ user\ pass\params \ userSource)	//注意 param从string已经被解析了
 * @param  {Function}   cb
 * @return {Void}
 */
service.auth = function(dBAuthInfo, cb) {
	var self = this;
	var MsgCode = bearcat.getBean('msgCode');

	if(this.isTooManyPlayers()) {
		var dbAuthRet = new (bearcat.getBean('dBMsgDefine').DBAuthRet)();
		dbAuthRet.code = MsgCode.TooManyPlayers;
		dbAuthRet.uid = -1;		
		self.utils.invokeCallback(cb, dbAuthRet);
		return;
	}	
	
	self.platformMgr.auth(dBAuthInfo, function(dbAuthRet) {//DBAuthRet
        kakassilog.info('AuthMgr:auth dbAuthRet:', dbAuthRet);
		self.utils.invokeCallback(cb, dbAuthRet);		
	});	
}

service.incPlayerNum = function() {	
	this.playerNum ++;	
	logger.info('players:{1}/{2}'.format(this.playerNum,this.maxPlayerNum));
	playernumlogger.info('players:{1}/{2}'.format(this.playerNum,this.maxPlayerNum));
}

service.decPlayerNum = function() {
	this.playerNum --;	
	logger.info('players:{1}/{2}'.format(this.playerNum,this.maxPlayerNum));
	playernumlogger.info('players:{1}/{2}'.format(this.playerNum,this.maxPlayerNum));
}

service.getPlayerNum = function() {
	return this.playerNum;	
}

service.setOnlineMax = function(onlineMax) {
	this.maxPlayerNum = onlineMax;	
}

service.isTooManyPlayers = function() {	
	return this.playerNum >= this.maxPlayerNum;
}

module.exports = {
	id: "authMgr",
	func: AuthMgr,
	init: "init"
}