var redis = require('redis');
var async = require('async');
var bearcat = require('bearcat');
var logger = require('pomelo-logger').getLogger('2048', 'RedisDBClient');
var dblog = require('pomelo-logger').getLogger('db');
var kakassilog = require('pomelo-logger').getLogger('kakassidebug');
var friendlog = require('pomelo-logger').getLogger('kakassidebug');


// 数据库层
var RedisDBClient = function() {
	this.client = null;	
	this.ready = false;		
	this.opt = {};
	this.utils = null;	
	this.config = {};
};

var ptype = RedisDBClient.prototype;

ptype.initConfig = function(opt) {
	this.opt.db = 0;
}

ptype.start = function(cb) {
	var self = this;	
	
	this.initConfig();
	this.utils = bearcat.getBean('utils');
	var config = bearcat.getBean('globalConfig').getRedisAndCenterConfig();

	var host = config.redisip;
	var port = config.redisport;
	var pass = '';
	var opts = {auth_pass:pass};

	dblog.info('connect to redis,ip:', host, ' port:', port);
	this.client = redis.createClient(port, host, opts);
	this.client.on('ready', function(err) {
		self.prepareDB(function() {
			self.utils.invokeCallback(cb);
		});
	});	
	this.client.on('error', function(err){ 
	  	//console.error('Redis error:', err);
	  	dblog.info('Redis error:',err); 
	});
}

ptype.stop = function(cb) {
	var self = this;

	self.client.quit(function() {
		self.utils.invokeCallback(cb);			
	});
	self.client = null;	
}

ptype.prepareDB = function(cb) {	
	// 系统信息
	var self = this;
	async.waterfall([
		function(callback) {
			self.client.select(self.opt.db, function(err, reply) {
				callback(null);
			});
		} ,function(callback) {			
			self.loadConfig(function() {
				callback(null);
			});
		}],
		function(arg0) {
			self.ready = true;
			self.utils.invokeCallback(cb);
		});	
}

ptype.loadConfig = function(cb) {
	var self = this;

	this.config.uid = 1;
	async.waterfall([
		function(callback) {
			// load config from some db
			self.client.get('config.uid', function(error, reply) {
				if( reply != null ) {
					self.config.uid = parseInt(reply);
					logger.info('got uid:', reply);
				}
				callback(null);
			});			
		}],
		function(err) {
			self.utils.invokeCallback(cb);			
		});	
}

// 没有user就自动注册一个
// args
// 		channel
// 		user
/**
 * RedisDBClient auth.
 *
 * @param  {DBAuthInfo}   args (channel\ user\ pass\params)	//注意 param从string已经被解析了
 * @param  {Function}   cb
 * @return {Void}
 */
ptype.auth = function(args, cb) {
	var MsgCode = bearcat.getBean('msgCode');
	var dbAuthRet = new (bearcat.getBean('dBMsgDefine').DBAuthRet)();

	if(!this.ready) {
		//this.utils.invokeCallback(cb,{uid:0,code:MsgCode.RedisNotReady});	
		if(cb)
			cb({uid:0,code:MsgCode.RedisNotReady});
		this.start();
		return;
	}

	var self = this;
	var client = this.client;	

	// user@channel
	var unique_user = this.makeAuthKey(args.channel, args.user);
	async.waterfall([
		function(callback) {
			client.hgetall(unique_user, function(err, reply) {
				if(reply == null) {
					needRegister = true;
					callback(null);
				}
				else {
					//logger.info('auth ack:', reply);
					dblog.info('auth ack:',reply); 
					callback(true, reply);
				}
			});
		},function(callback) {
			// 注册帐号
			self.register(args, function(obj) {
				callback(null,obj);
			});
		}]
		,function(err, obj) {
			dbAuthRet.uid = parseInt(obj.uid);
			dbAuthRet.code = MsgCode.Succ;
			self.utils.invokeCallback(cb, dbAuthRet);		
		});
}

/**
 * RedisDBClient register.
 * 帐号注册
 * @param  {DBAuthInfo}   args (channel\ user\ pass\params)	//注意 param从string已经被解析了
 * @param  {Function}   cb
 * @return {Void}
 */
ptype.register = function(args, cb) {
	var self = this;
	var unique_user = this.makeAuthKey(args.channel, args.user);
	var obj = {};
	obj.uid = self.allocUId();
	obj.channel = args.channel;
	obj.user = args.user;

	var params = args.params;

	dblog.info('register:', obj );
	dblog.info('args:', args );

	async.waterfall([function(callback) {
			self.client.multi()
			.hmset(unique_user, obj)
			.set('config.uid',self.config.uid)
			.exec(function(err, replies) {
				logger.info('replies:', replies );
				callback(null);
			});
		},function(callback) {
			callback(null);
			return;
			//暂时就不存数据库了

			// save to db
    		var name = self.utils.makeDefaultName(uid);
			var dbReqSavePlayer = bearcat.getBean('dbMgr').makeInitPlayerInfo(dbAuthInfo, uid,
				unique_user, name);
			self.savePlayer(dbReqSavePlayer, function(ret) {
				callback(null);
			});
			
		}],function(err) {
			self.utils.invokeCallback(cb, obj);
		});
}

ptype.allocUId = function() {
	dblog.info('config:', this.config);
	var uid = this.config.uid;
	this.config.uid = this.config.uid + 1;	
	return uid;
}

/**
 * RedisDBClient makeAuthKey.
 * 账号名@渠道名
 * @param  {String}  channel
 * @param  {String}   user
 * @return {String} user@channel
 */
ptype.makeAuthKey = function (channel, user) {
	var mgr = bearcat.getBean('platformMgr');
	return mgr.makeAuthKey(channel, user);
}

ptype.makePlayerInfoKey = function(uid) {
	return 'user.' + uid;
}

// .uid
// .info
ptype.savePlayer = function(dbReqSavePlayer, cb) {
	var self = this;
	dblog.info('savePlayer:', dbReqSavePlayer);

	var dbAckSavePlayer = new (bearcat.getBean('dBMsgDefine').DBAckSavePlayer)();

	if(!self.ready) {
		dbAckSavePlayer.result = false;
		self.utils.invokeCallback(cb, dbAckSavePlayer);
		return;
	}

	var key = self.makePlayerInfoKey(dbReqSavePlayer.uid);	//key = 'user.id'
	self.client.hmset(key, {'info': dbReqSavePlayer.info}, function(err, reply) {
		if( err )
			dblog.error('redis err:', err);
		dbAckSavePlayer.result = true;
		self.utils.invokeCallback(cb, dbAckSavePlayer);
	});
}

// uid
// 返回
// 		info:角色信息
// 		account:信息
/**
 * RedisDBClient loadPlayer.
 * 载入玩家数据
 * @param  DBReqLoadPlayer（uid） args
 * @param  {Function}  cb
 * @return {Void}
 */
ptype.loadPlayer = function(args, cb) {
	var self = this;
	if(!self.ready) {
		self.utils.invokeCallback(cb, {result:false});
		return;
	}

	var key = self.makePlayerInfoKey(args.uid);
	self.client.hgetall(key, function(err, reply) {
		dblog.info('loadPlayer:', reply);
		var playerInfo = null;
		if(reply)
			playerInfo = reply['info'];
		var dBMsgDefine = bearcat.getBean('dBMsgDefine');
    	var dBAck = new dBMsgDefine.DBAckLoadPlayer();
    	dBAck.result = true; 
    	dBAck.playerInfo = playerInfo;
		self.utils.invokeCallback(cb, dBAck);
	});
}



ptype.makeNameKey = function(name) {
	return 'name.' + name;
}

// args
// 		.name
// 		.uid
ptype.createName = function(args, cb) {
	// 判断该名字是否存在
	var self = this;
	var MsgCode = bearcat.getBean('playerInfoFactory').commonConsts.MsgCode;
	var key = this.makeNameKey(args.name);
    
    if(!self.ready) {
        kakassilog.info('RedisDBClient createName Err : RedisNotReady');
        self.utils.invokeCallback(cb, {code:MsgCode.RedisNotReady});
        return;
    }

	async.waterfall([function(callback) {
		self.client.hgetall(key, function(err, reply) {
			dblog.info('check name exsit:', reply);
			if(reply) {
				callback(true, MsgCode.SetNameErr_Duplicate);
				return;
			}	
			callback(null);		
		});
	}, function(callback) {		
		// set name
		var info = {};
		info.uid = args.uid;
		var str = JSON.stringify(info);
		self.client.hmset(key, {'info': str}, function(err, reply) {
			if( err )
				dblog.error('redis err:', err);
			// 取名字成功
			callback(true, MsgCode.Succ);
		});
	}],function(err, code) {
		self.utils.invokeCallback(cb, {code:code});
	});	
}

ptype.makeCacheKey = function(key) {
	return 'c.' + key;
};


///////////////////////
///redis 的各种接口
///

// hash set
// hmset 封装的接口 将多个字段-值对设置到哈希表中
ptype.hashMulSet = function(key,info,cb) {
    var self = this;    
    if(!self.ready){
        friendlog.info('RedisDBClient:hashMulSet err: not ready');   
        self.utils.invokeCallback(cb, {result:false});
        return;
    }

    self.client.hmset(key, info , function(err, reply) {
        friendlog.info('RedisDBClient:hashMulSet ret:err=', err,'reply=',reply);   
        if( err ){
            friendlog.error('RedisDBClient:redis err:', err);
            self.utils.invokeCallback(cb, {result:false});
        }
        self.utils.invokeCallback(cb,err); //暂时不需要返回数据
    }); 
}

// hash get
// hgetall 封装的接口 根据key 到对应的hash表中 将所有字段-值对 取出 返回
ptype.hashGetAll = function(key,cb) {
    var self = this;  
    if(!self.ready) {
        friendlog.info('RedisDBClient:hashGetAll err: not ready');   
        self.utils.invokeCallback(cb, {result:false});
        return;
    }

    self.client.hgetall(key, function(err, reply) {
        self.utils.invokeCallback(cb,err,reply);
    });

}
// 对zrevrange的封装
// 从有序集合中 逆序取得数据 （指定范围内）
// start 起点
// stop 终点
ptype.getRevRangeFromSortedSet = function(key,start,stop,cb) {
    var self = this;  
    if(!self.ready) {
        friendlog.info('RedisDBClient:getRevRangeFromSortedSet err: not ready');   
        self.utils.invokeCallback(cb, {result:false});
        return;
    }
    self.client.zrevrange(key,start,stop,'withscores',function(err, reply) {
        self.utils.invokeCallback(cb,err,reply);
    });
}

//zadd 封装 
//插入有序队列
//参数必须是如下规格 args:[key, score, uid]
ptype.addToSortedSet = function(args,cb) {
    var self = this;     
    if(!self.ready){
        self.utils.invokeCallback(cb, {result:false});
        return;
    }

    self.client.zadd(args, function(err, reply) {
        if( err )
            friendlog.error('addToSortedSet redis err:', err);
        self.utils.invokeCallback(cb, err,reply);
    });
}

//zrem 封装 
//从有序队列中删除
//
ptype.removeFromSortedSet = function(key,value,cb) {
    var self = this;
    if(!self.ready){
        self.utils.invokeCallback(cb, {result:false});
        return;
    }

    self.client.zrem(key,value, function(err, reply) {
        if( err )
            friendlog.error('removeFromSortedSet redis err:', err);
        self.utils.invokeCallback(cb, err);
    });
}

//zrevrank 的封装
//获得有序序列中 uid对应的排名 （逆序）
//参数必须是 key 和 value
ptype.getRevRankFromSortedSet = function(key,uid,cb) {
    var self = this;     
    if(!self.ready) {
        self.utils.invokeCallback(cb, 'not ready');  //此处self是否应该学需要传入
        return;
    }
    self.client.zrevrank(key,uid,function(err, reply) {
        friendlog.info('RedisDBClient getRevRankFromSortedSet err:',err,',key=',key,',uid=',uid);   
        friendlog.info('RedisDBClient getRevRankFromSortedSet reply:',reply);
        self.utils.invokeCallback(cb, err,reply);
    });                     
}

//zcard 封装 
//获得有序集合中元素的数量
//参数必须是如下规格 key
ptype.getCountInSortedSet = function(key,cb) {
    var self = this;     
    if(!self.ready){
        self.utils.invokeCallback(cb, {result:false});
        return;
    }

    self.client.zcard(key, function(err, reply) {
        if( err )
            friendlog.error('addToSortedSet redis err:', err);
        self.utils.invokeCallback(cb, err, reply);
    });
}

/////////
///


//addToSet 
//对sadd 的封装 插入到 set
ptype.addToSet = function(key,value,cb) {
    var self = this;     
    if(!self.ready) {
        self.utils.invokeCallback(cb, 'not ready');  //此处self是否应该学需要传入
        return;
    }
    self.client.sadd(key,value,function(err, reply) {
        friendlog.info('RedisDBClient addToSet err:',err,',key=',key,',value=',value);   
        friendlog.info('RedisDBClient addToSet reply:',reply);
        self.utils.invokeCallback(cb, err,reply);
    });                     
}

//RemoveFromSet
//srem 的封装 从set中移除
ptype.removeFromSet = function(key,value,cb) {
    var self = this;     
    if(!self.ready) {
        self.utils.invokeCallback(cb, 'not ready');  //此处self是否应该学需要传入
        return;
    }
    self.client.srem(key,value,function(err, reply) {
        friendlog.info('RedisDBClient RemoveFromSet err:',err,',key=',key,',value=',value);   
        friendlog.info('RedisDBClient RemoveFromSet reply:',reply);
        self.utils.invokeCallback(cb, null,reply);
    });                     
}

//getAllFromSet
//smembers 的封装 从set中获得所有元素
ptype.getAllFromSet = function(key,cb) {
    var self = this;     
    if(!self.ready) {
        self.utils.invokeCallback(cb, 'not ready');  //此处self是否应该学需要传入
        return;
    }
    self.client.smembers(key,function(err, reply) {
        friendlog.info('RedisDBClient getAllFromSet err:',err,',key=',key);   
        friendlog.info('RedisDBClient getAllFromSet reply:',reply);
        self.utils.invokeCallback(cb, null,reply);
    });                     
}



ptype.set = function(key,value,cb) {
    var self = this;     
    if(!self.ready) {
        self.utils.invokeCallback(cb, 'not ready');  //此处self是否应该学需要传入
        return;
    }
    self.client.set(key,value,function(err, reply) {
        friendlog.info('RedisDBClient set err:',err,',key=',key);   
        friendlog.info('RedisDBClient set reply:',reply);
        self.utils.invokeCallback(cb, null,reply);
    });                     
}

ptype.get = function(key,cb) {
    var self = this;     
    if(!self.ready) {
        self.utils.invokeCallback(cb, 'not ready');  //此处self是否应该学需要传入
        return;
    }
    self.client.get(key,function(err, reply) {
        friendlog.info('RedisDBClient get err:',err,',key=',key);   
        friendlog.info('RedisDBClient get reply:',reply);
        self.utils.invokeCallback(cb, null,reply);
    });                     
}

module.exports = {
	id: "redisDBClient",
	func: RedisDBClient
}

