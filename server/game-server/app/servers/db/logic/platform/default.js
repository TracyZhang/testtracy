var async = require('async');
var util = require('util');
var Platform = require('./platform');

var Default = function() {	
	Platform.call(this);
	this.channel = 'default';
	this.utils = null;
	this.dbMgr = null;
    this.userSource = 18158;
};
util.inherits(Default, Platform);

var ptype = Default.prototype;

ptype.init = function() {	
	Platform.prototype.init.call(this);

	this.utils = bearcat.getBean('utils');
	//this.dbMgr = bearcat.getBean('dbMgr');
    //本项目不再存在dbMgr 直接使用redis
    this.redis =  bearcat.getBean('redisDBClient');
}

// 认证
/**
 * Default auth.
 *
 * @param  {DBAuthInfo}   args (channel\ user\ pass\params) //注意 param从string已经被解析了
 * @param  {Function}   cb
 * @return {Void}
 */ 
ptype.auth = function(dBAuthInfo, cb) {//DBAuthInfo
    var self = this;

    dBAuthInfo.channelUId = dBAuthInfo.user;
    dBAuthInfo.userSource = this.userSource;
	this.redis.auth(dBAuthInfo, function(dbAuthAck) {//DbAuthAck
        dbAuthAck.channel = dBAuthInfo.channel;        
        dbAuthAck.userSource = this.userSource;
        self.utils.invokeCallback(cb, dbAuthAck);
    });	
}

// 付费
ptype.pay = function(reqPay, cb) {
    var platformMgr = bearcat.getBean('platformMgr');    
    
    async.waterfall([function(callback) {
        platformMgr.defaultPay(reqPay, function(ackPay) {                        
            callback(null, ackPay);
        });    
    }, function(ackPay, callback) {
        // delay 伪造一个订单状态
        setTimeout(function() {
            platformMgr.makeFakeRetOrder(reqPay);
            cb(ackPay);
        }, 1000);
    }], function(err) {        
    });
}

// 平台推送过来事件
ptype.onEvent = function(args) {
}

module.exports = Default;

