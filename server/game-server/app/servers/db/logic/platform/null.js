var util = require('util');
var Platform = require('./platform');


var NullPlatform = function() {	
	Platform.call(this);
	this.channel = 'null';	
};
util.inherits(NullPlatform, Platform);

var ptype = NullPlatform.prototype;

ptype.init = function() {	
	Platform.prototype.init.call(this);
	this.utils = bearcat.getBean('utils');
}

// 认证
ptype.auth = function(args, cb) {
	var	uid = args.user;
	var rets = {uid:uid,result:true};
	this.utils.invokeCallback(cb, rets);
}

// 付费
ptype.pay = function(args, cb) {

}

// 平台推送过来事件
ptype.onEvent = function(args) {
}

module.exports = NullPlatform;

