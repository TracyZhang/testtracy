var Default = require('./default');
var Null = require('./null');
var PlatformRegister = function() {	
	
};

var ptype = PlatformRegister.prototype;

ptype.register = function(mgr, obj) {
	obj.init();
	mgr.registerHandler(obj.channel, obj);
}

// 认证
ptype.registerAll = function(mgr) {
	this.register(mgr, new Default());
	this.register(mgr, new Null());
}

module.exports = PlatformRegister;


