var logger = require('pomelo-logger').getLogger('2048', 'PlatformMgr');
var bearcat = require('bearcat');
var pomelo = require('pomelo');
var PlatformRegister = require('./PlatformRegister');

var PlatformMgr = function() {
	this.utils = null;		
	this.handlers = {}
	this.defaultChannel = null;

	logger.info('PlatformMgr');
};

var ptype = PlatformMgr.prototype;

ptype.init = function() {	
}

ptype.start = function() {
	this.app = pomelo.app;
	this.utils = bearcat.getBean('utils');
	new PlatformRegister().registerAll(this);	

	var defaultChannelName = 'default';
	this.defaultChannel = this.rawGetHandler(defaultChannelName);
	if(!this.defaultChannel) {
		logger.error('platform:default channel not set!');
		//bearcat.getBean('sutils').printStack(99);
	}
}

ptype.registerHandler = function(channel, handler) {
	this.handlers[channel] = handler;	
}

ptype.rawGetHandler = function(channel) {
	return this.handlers[channel];
}

ptype.getHandler = function(channel) {
	var handler = this.handlers[channel];
	if(!handler)
		handler = this.defaultChannel;
	return handler;
}

// .channel
// .user
// .pass
/**
 * PlatformMgr auth.
 *
 * @param  {DBAuthInfo}   args (channel\ user\ pass\params)	//注意 param从string已经被解析了
 * @param  {Function}   cb
 * @return {Void}
 */
ptype.auth = function(dbAuthInfo, cb) {

	if (this.defaultChannel == null) {
		this.start();
	}

	var channel = dbAuthInfo.channel;
	logger.info('channel:', channel);
	this.getHandler(channel).auth(dbAuthInfo, cb);
}

ptype.getAuthChannelByChannel = function(channel) {
	// otaku和jump8共用角色
	if(channel == 'otaku')
		return 'jump8';
	return channel;
}

ptype.makeAuthKey = function (channel, user) {
	return user + '@' + this.getAuthChannelByChannel(channel);
}

ptype.pay = function(reqPay, cb) {
	var channel = reqPay.channel;
	this.getHandler(channel).pay(reqPay, cb);
}

ptype.defaultPay = function(reqPay, cb) {
	var msgDefine = bearcat.getBean('dBMsgDefine');
	var config = bearcat.getBean('globalConfig').getConfig();
	var MsgCode = bearcat.getBean('playerInfoFactory').commonConsts.MsgCode;
	var guid = parseInt(config.worldguid);
	var ackPay = new msgDefine.AckPay();
	var extArgs = ackPay.extArgs;

	extArgs.payId = reqPay.payId;
	extArgs.orderId = reqPay.orderId;
	extArgs.guid = guid;
	extArgs.uid = reqPay.uid;

	ackPay.code = MsgCode.Succ;

	cb(ackPay);
}

ptype.makeFakeRetOrder = function(reqPay) {
	var msgDefine = bearcat.getBean('dBMsgDefine');	
	var MsgCode = bearcat.getBean('playerInfoFactory').commonConsts.MsgCode;

	var reqUpdate = new msgDefine.DBReqUpdatePay();
	reqUpdate.orderId = reqPay.orderId;	
	reqUpdate.state = msgDefine.PAY_STATE_FLAG.WAIT_RECEIVE;	

	// TODO: payCenter服务器准备好了以后, 流程可以从那边开始
	bearcat.getBean('paymentMgr').makeFakeRetOrder(reqUpdate, function() {
		logger.info('makeFakeRetOrder ret')		
	});
}

module.exports = {
	id: "platformMgr",
	func: PlatformMgr,
	init: "init"
}