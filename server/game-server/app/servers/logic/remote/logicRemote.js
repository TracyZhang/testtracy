var async = require('async');
var logger = require('pomelo-logger').getLogger('2048', __filename, process.pid);
var kakassilog = require('pomelo-logger').getLogger('kakassidebug');
var matchlog = require('pomelo-logger').getLogger('matchlog');
var formatlogger = require('pomelo-logger').getLogger('format');
var path = require('path');

var LogicRemote = function (app) {
	this.app = app;
	this.utils = null;
	this.consts = null;
	this.channelService = app.get('channelService');
	this.playerMgr = bearcat.getBean('playerMgr');
}

var handler = LogicRemote.prototype;

/**
 * LogicRemote enter.
 * 进入游戏世界
 * @param  {MsgPlayerEnter} 
 * @param  {Function} cb   
 * @return {Void}
 */
handler.enterLogic = function (msgPlayerEnter, cb) {	//MsgPlayerEnter
	var self = this;

	//正常登陆
	this.playerEnter(this, msgPlayerEnter, function (player) {
		player.onEnterWorld( function () {
			self.utils.invokeCallback(cb, { succ: true });
		});
	});


};


/**
 * LogicRemote playerEnter.
 * 进入游戏世界
 * @param  self
 * @param  {MsgPlayerEnter}  msgPlayerEnter
 * @return {Void}
 */
handler.playerEnter = function (self, msgPlayerEnter, cb) {
	
	var uid = msgPlayerEnter.uid;
	var frontendId = msgPlayerEnter.frontendId;
	var player = bearcat.getBean('player', {
		uid: uid,
		frontendId: frontendId
	});

	self.playerMgr.addPlayer(player);

	var dBMsgDefine = bearcat.getBean('dBMsgDefine');
    var dBReqLoadPlayer = new dBMsgDefine.DBReqLoadPlayer();
    dBReqLoadPlayer.uid = uid; 

	//载入玩家数据 ret DBAckLoadPlayer
	//player.setLoading();
	self.app.rpc.db.dbRemote.loadPlayer(null, dBReqLoadPlayer, function (ackLoadPlayer) {

		setTimeout( function() {
			doLoadPlayer();
		}, 1);

		var doLoadPlayer = function() {
			//player.setOnline();
			if (ackLoadPlayer.result) {				
				player.loadPlayerInfo(ackLoadPlayer.playerInfo);						
				//player.setLoginTime(new Date().getTime());
                player.onLoadPlayerInfoSuccess();                //

			}
			else {
				logger.error('load player info error!');
				player.kickLater(3000);
			}
			self.utils.invokeCallback(cb, player);		
		}		
	});
}

handler.reqOffline = function (args, cb) {
	var self = this;
	this.playerOffline(self, args.uid, function () {
		self.utils.invokeCallback(cb);
	});
}

/**
 * LogicRemote playerOffline.
 * 玩家下线 
 * @param  self
 * @param  {Number} uid 玩家id
 * @param  {BOOL} isSwitch  是否是切线
 * @param  {PlayerEnterReqLogicDtoLogic}  args
 * @return {Void}
 */
handler.playerOffline = function (self, uid, cb) {	
	//matchlog.info("LogicRemote:playerOffline:uid = ", uid);
	var player = self.playerMgr.getPlayer(uid);
	if(!player) {
		self.utils.invokeCallback(cb);
		return;
	}

	//if(player.isLoading()) {
	//	matchlog.info('LogicRemote:playerOffline: got offline when loading..., skip it');
	//	self.utils.invokeCallback(cb);
	//	return;
	//}


    if (player.isDBValid() && player.isDBDirt()) {
	    var dbReqSavePlayer = new (bearcat.getBean('dBMsgDefine').DBReqSavePlayer)();
	    dbReqSavePlayer.uid = player.uid;
	    dbReqSavePlayer.info = player.makePlayerInfo();
	    dbReqSavePlayer.brief = player.makeBriefInfo();
	    self.app.rpc.db.dbRemote.savePlayer(null, dbReqSavePlayer, function (ret) {
	        self.utils.invokeCallback(cb);
	    });
	}
	else
	    self.utils.invokeCallback(cb);
}



module.exports = function (app) {
	var ret = bearcat.getBean({
		id: 'logicRemote',
		func: LogicRemote,
		args: [{
			name: 'app',
			value: app
		}],
		props: [{
			name: 'utils',
			ref: 'utils'
		}, {
			name: 'consts',
			ref: 'consts'
		}]
	});
	return ret;
};