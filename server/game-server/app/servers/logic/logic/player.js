var logger = require('pomelo-logger').getLogger('2048', 'Player');
var formatlogger = require('pomelo-logger').getLogger('format');
var pomelo = require('pomelo');
var bearcat = require('bearcat');
var util = require('util');
var async = require('async');


function Player(opts) {
    //var Entity = bearcat.getFunction('entity');
    //Entity.call(this);

    // uid and serverid
    this.uid = opts.uid;
    this.frontendId = opts.frontendId;
    this.utils = null;
    this.consts = null;

    // 非正常初始化的player,不存储 
    this.dbValid = false;
    this.dbDirt = false;

    //this.playerInfoFactory = bearcat.getBean('playerInfoFactory');

    this.playerSystemAgent = bearcat.getBean('playerSystemAgent');
    this.playerSystemAgent.initPlayerSystemAgent(this);

   
}

var handler = Player.prototype;

handler.init = function() {
    this.playerSystemAgent.initSystems();
}

handler.getUId = function() {
    return this.uid;
}

handler.setName = function(v) {
    //this.playerInfoAgent.setName(v);
}

handler.getName = function() {
    //return this.playerInfoAgent.getName();
}

handler.getSystem = function(systemName) {
    return this.playerSystemAgent.getSystem(systemName);
}


handler.loadPlayerInfo = function(info) {
    if (info) {
        //var jsonObj = LUtils.jsonParse(info);
        var jsonObj = JSON.parse(content);
        this.playerSystemAgent.loadSystems(jsonObj);
    } else {
        this.playerSystemAgent.loadSystems({});
    }

    this.setDBValid(true);
}

handler.onLoadPlayerInfoSuccess = function() {
}

handler.onEnterWorld = function(cb) {
    // 通知各個系統 上綫成功
    //this.sendInfoToClient(cb);
    //this.emit('onEnterWorld');
    this.playerSystemAgent.onEnterWorld(); 
}


handler.makePlayerInfo = function() {
    var LUtils = this.playerInfoFactory.LUtils;
    var jsonObj = {};
    this.playerSystemAgent.saveSystems(jsonObj);
    return  JSON.stringify(jsonObj);
    //kakassilog.info("makePlayerInfo:uid="+this.uid+",name="+this.getName()); 
    //return LUtils.jsonStringify(jsonObj);
}

handler.setDBValid = function(valid) {
    this.dbValid = valid;
}

handler.isDBValid = function() {
    return this.dbValid;
}

handler.setDBDirt = function(dirt) {
    this.dbDirt = dirt;
}

handler.isDBDirt = function() {
    return this.dbDirt;
}

handler.sendMsgToClient = function(msgName, param, cb) {
    var self = this;
    var channelService = pomelo.app.get('channelService');
    channelService.pushMessageByUids(msgName, param, [{
        uid: self.uid,
        sid: self.frontendId
    }], function(error) {
        self.utils.invokeCallback(cb, error);
        //kakassidebuglog.info("sendMsgToClient CB: msgName = ",msgName,"self = ",self,"this = ",this);
    });
}

// handler.sendInfoToClient = function(cb) {
//     this.sendMsgToClient('playerInfo', this.makeClientPlayerInfo(), cb);
//     var now = new Date().getTime();
//     this.sendMsgToClient('serverTime',{time:now} , null);
// }




/**
 * Player onGoldChange 
 * @param    info 
 * @return 
 * @api public
 */
handler.onGoldChange = function(info) {
    this.emit('onGoldChange', info);
}


/**
 * Player onDiamondChange 
 * @param    info 
 * @return 
 * @api public
 */
handler.onDiamondChange = function(info) {
    this.emit('onDiamondChange', info);
}


handler.onLeaveWorld = function(cb) {
    logger.info('onLeaveWorld');
    this.emit('onLeaveWorld');   
    this.utils.invokeCallback(cb);
}

handler.clear = function() {
    //this.systems.clear();
    this.playerSystemAgent.clear();
}

handler.kick = function() {
    logger.info('kick:', this.uid);

    // 踢下线   
    if (this.isOnline()) {
        var app = pomelo.app;
        app.backendSessionService.kickByUid(this.frontendId, this.uid, function() {
            logger.info('kick ret');
        });
    } else {
        var logicRemote = bearcat.getBean('logicRemote');

        var player = this;
        logicRemote.offlinePlayerRelease(player, null);
    }
}

// 延迟一段时间踢出
handler.kickLater = function(delay) {
    var self = this;
    setTimeout(function() {
        self.kick();
    }, delay);
}

handler.onSystemReq = function(msg) {
    this.playerSystemAgent.onSystemReq(msg);
}

//此接口必须要有cb
handler.onSystemReqCb = function(msg,cb) {
    this.playerSystemAgent.onSystemReqCb(msg,function(ret){
        if(cb){cb(ret);}
    });
}

handler.onCmd = function(cmd) {
    cmdMgr.execute(this, cmd);
}

module.exports = {
    id: "player",
    func: Player,
    scope: "prototype",
    parent: "entity",
    init: "init",
    args: [{
        name: "opts",
        type: "Object"
    }],
    props: [{
        name: "consts",
        ref: "consts"
    }, {
        name: "utils",
        ref: "utils"
    }]
}