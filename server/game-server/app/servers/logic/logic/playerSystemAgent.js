var logger = require('pomelo-logger').getLogger('2048', 'Player');
var formatlogger = require('pomelo-logger').getLogger('format');
var pomelo = require('pomelo');
var bearcat = require('bearcat');
var util = require('util');
var async = require('async');
var kakassilog = require('pomelo-logger').getLogger('kakassidebug');

//系统

function PlayerSystemAgent() {
    this.player = null;
    ///this.playerInfoFactory = bearcat.getBean('playerInfo');
    this.utils = null;
    this.consts = null;

    // name:systemHandler
    //this.systems = null;
    this.systems = {};
}

var __proto = PlayerSystemAgent.prototype;

__proto.initPlayerSystemAgent = function(player) {   
    this.player = player;
}    

__proto.initSystems = function() {   
    this.addSystem('testSystemHandler'); 
}    

__proto.addSystem = function(systemBeanName) {
    //kakassilog.info('addSystem',systemBeanName);    
    var systemHandler = bearcat.getBean(systemBeanName);
    if (!systemHandler){
        //kakassilog.info('addSystem fail',systemBeanName);    
        return;
    }
    systemHandler.init(this.player);
    this._addSystem(systemHandler);
}

__proto.getSystem = function(name) {
    var system = this.systems[name];
    if(!system)
        return null;
    return system;
}

__proto.loadSystems = function(jsonObj) {
    var systemsJsonObj = jsonObj['systems'];
    if (!systemsJsonObj)
        systemsJsonObj = {};
    this._loadSystems(systemsJsonObj);
}

__proto.saveSystems = function(jsonObj) {
    jsonObj['systems'] = {};
    this._saveSystems(jsonObj['systems']);
}


__proto.clear = function() {
    this.player = null;

    var keys = Object.keys(this.systems);
    for(var i = 0; i < keys.length; i ++) {
        var name = keys[i];     
        var system = this.getSystem(name);
        if(system) {
            system.clear();         
        }
    }
    this.systems = null;
}

__proto.onSystemReq = function(msg) {  
    //kakassilog.info("PlayerSystemAgent:onSystemReq:system=",msg.system,",cmd=",msg.cmd,",args=",msg.args);  
    var system = this.getSystem(msg.system);
    if(!system){
        //kakassilog.info("PlayerSystemAgent:onSystemReqErr!!!:system=",msg.system,",cmd=",msg.cmd,",args=",msg.args);      
        return;
    }
    system.onSystemReq(msg.cmd, msg.args);
}

//此接口必须要有cb
__proto.onSystemReqCb = function(msg,cb) {  
    //kakassilog.info("PlayerSystemAgent:onSystemReqCb:system=",msg.system,",cmd=",msg.cmd,",args=",msg.args);  
    var system = this.getSystem(msg.system);
    if(!system){
        //kakassilog.info("PlayerSystemAgent:onSystemReqErr!!!:system=",msg.system,",cmd=",msg.cmd,",args=",msg.args);   
        if(cb){cb();}   
        return;
    }
    system.onSystemReqCb(msg.cmd, msg.args,function(ret){
        if(cb){cb(ret);}  
    });
}



__proto._addSystem = function(systemHandler) { 
    this.systems[systemHandler.getSysName()] = systemHandler;
}

__proto._loadSystems = function(jsonObj) {
    var keys = Object.keys(this.systems);
    for(var i = 0; i < keys.length; i ++) {
        var name = keys[i];
        var childObj = jsonObj[name]; 
        var system = this.getSystem(name);
        if(system) {
            system.loadFromJsonObj(childObj);
            system.onLoadOver();
        }
    }
}

__proto._saveSystems = function(jsonObj) {
    var keys = Object.keys(this.systems);
    for(var i = 0; i < keys.length; i ++) {
        var name = keys[i];     
        var system = this.getSystem(name);
        if(system) {
            jsonObj[name] = {};
            system.saveToJsonObj(jsonObj[name]);
        }
    }
}

__proto.updateSystems = function() {
    var keys = Object.keys(this.systems);
    for(var i = 0; i < keys.length; i ++) {
        var name = keys[i];     
        var system = this.getSystem(name);
        if(system) {            
        }
    }
}


__proto.onEnterWorld = function() {
    var keys = Object.keys(this.systems);
    for(var i = 0; i < keys.length; i ++) {
        var name = keys[i];     
        var system = this.getSystem(name);
        if(system) {   
            system.onEnterWorld();             
        }
    }
}

module.exports = {
    id: "playerSystemAgent",
    func: PlayerSystemAgent,
    scope: "prototype",
    props: [{
        name: "consts",
        ref: "consts"
    }, {
        name: "utils",
        ref: "utils"
    }]
}