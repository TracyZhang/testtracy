var SysUtil = require('util');
var async = require('async');
var BaseSystem = require('./BaseSystem');
var logger = require('pomelo-logger').getLogger('2048', 'ExampleSystemHandler');
var kakalog = require('pomelo-logger').getLogger('kaka');
var friendlog = require('pomelo-logger').getLogger('friend');
var pomelo = require('pomelo');
var BaseSystemHandler = BaseSystem.BaseSystemHandler;

// ------------------------------------------------
// 简明信息系统 此系统不存储任何数据 也不与客户端通信 仅仅负责同步简明数据给redis
// 忽然想到 之前排行榜的score变化的时候 但是整体rank没有变化 其实是可以通过这边直接刷 连同rankinfo一起刷
// 
// 
var TestSystemHandler = function() {	
    BaseSystemHandler.call(this);
	this.sysName = 'test';
    this.utils = bearcat.getBean('utils');
}

SysUtil.inherits(TestSystemHandler, BaseSystemHandler);
var __proto = TestSystemHandler.prototype;
__proto.init = function(player) {
	this.player = player;
    kakalog.info('TestSystemHandler init');
    //暂时没有引入事件 所以。。。 
    //player.on('onEnterWorld', this, this.onEnterWorld);
 
}

__proto.clear = function() {
    var player = this.player;
    BaseSystemHandler.prototype.clear.call(this);
}





// ------------------------------------------------
module.exports = {
    id: "testSystemHandler",
    func: TestSystemHandler,
    scope: "prototype"
}