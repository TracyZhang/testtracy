var SysUtil = require('util');


//var BaseData = Game.Loader.BaseData;

var logger = require('pomelo-logger').getLogger('2048', 'BaseSystem');
// 负责读取和存储数据
// BaseData
var BaseSystemDataEntry = function() {	
}

//SysUtil.inherits(BaseSystemDataEntry, BaseData);
var __proto = BaseSystemDataEntry.prototype;

// ------------------------------------------------

// 系统逻辑处理者
// 子系统的数据单独决定如何发给客户端
var BaseSystemHandler = function() {	
	// 系统唯一名
	this.sysName = '';
	this.player = null;
	// 继承者自己构造
	this.dataEntry = null;
}

var __proto = BaseSystemHandler.prototype;
__proto.init = function(player) {
	this.player = player;

	// 注册事件
}

__proto.clear = function() {	
	// 取消事件
	this.player = null;
	this.dataEntry = null;
}

__proto.isSystemValid = function() {
	return this.player != null;
}

__proto.getSysName = function() {
	return this.sysName;
}

// load from
__proto.loadFromJsonObj = function(jsonObj) {
	if(jsonObj && this.dataEntry)
		this.dataEntry.loadFromJsonObj(jsonObj);		
}

__proto.saveToJsonObj = function(jsonObj) {
	if(this.dataEntry)
		this.dataEntry.saveToJsonObj(jsonObj);
}

__proto.onLoadOver = function() {	
}

// 统一转发给系统来处理
__proto.onSystemReq = function(cmd, args) {	
}

// 统一转发给系统来处理 //此接口必须要有cb
__proto.onSystemReqCb = function(cmd, args,cb) {	
	if(cb){cb();}  
}

// 发送消息给客户端
__proto.sendSystemCmd = function(cmd, args) {
	if(!this.player)
		return;
	var msg = {};
	msg.system = this.getSysName();
	msg.cmd = cmd;
	msg.args = args;

	this.player.sendMsgToClient('systemCmd', msg, null);
}

__proto.update = function() {	
}


module.exports = {
    BaseSystemDataEntry: BaseSystemDataEntry,
    BaseSystemHandler: BaseSystemHandler
}