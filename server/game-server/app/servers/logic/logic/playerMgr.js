var logger = require('pomelo-logger').getLogger('2048', 'PlayerMgrLogic');
var bearcat = require('bearcat');
var pomelo = require('pomelo');

// uid:player
var PlayerMgrLogic = function() {	
	this.players = {}; //Player
};

var service = PlayerMgrLogic.prototype;

service.createPlayer = function() {
	return bearcat.getBean('player');
};

service.addPlayer = function(player) {
	this.players[player.uid] = player;    
    
    //var app = pomelo.app;
    //app.rpc.db.dbRemote.incPlayerNum(null, null, function() {});
};

service.getPlayer = function(uid) {
    //logger.info('getPlayer ',uid);  
    //logger.info('this.players =  ',this.players);  
    return this.players[uid];
};

service.removePlayer = function(uid) {
	delete this.players[uid];

    //var app = pomelo.app;
    //app.rpc.db.dbRemote.decPlayerNum(null, null, function() {});
}

service.forEachPlayer = function(callfun) {
    var keys = Object.keys(this.players);
    for(var k = 0; k<keys.length; k++){
        var one = this.players[keys[k]];
        callfun(one);
    }
}



module.exports = {
	id: "playerMgr",
	func: PlayerMgrLogic
}