var logger = require('pomelo-logger').getLogger('2048', 'Player');
var formatlogger = require('pomelo-logger').getLogger('format');
var pomelo = require('pomelo');
var bearcat = require('bearcat');
var util = require('util');
var async = require('async');

//玩家的基础信息

function PlayerInfoAgent() {
    this.player = null;
    this.utils = null;
}

var handler = PlayerInfoAgent.prototype;

handler.initPlayerInfoAgent = function (player) {
    this.player = player;

    this.utils = bearcat.getBean('utils');
    this.playerInfo = bearcat.getBean('playerInfo');
    var SPlayerInfo = this.playerInfo.SPlayerInfo;
    this.playerInfo = new SPlayerInfo();
}


handler.loadPlayerInfo = function (jsonObj) {
    this.playerInfo.loadFromJsonObj(jsonObj);
}

handler.saveToJson = function (jsonObj) {
    this.playerInfo.saveToJsonObj(jsonObj);
}

//其实这部分内容完全可以写在具体系统里
//=========

handler.addExp = function (exp) {
    var base = this.playerInfo.base;
    var maxExp = this.getMaxExp(base.level);

    base.exp += exp;
    if (maxExp > 0 && base.exp >= maxExp) {
        if (base.level < this.getMaxLevel()) {
            base.exp -= maxExp;
            base.level += 1;
            //formatlogger.info(',LevelUp:uid=',this.uid,',username=',this.getName(),',level=',base.level);
            var formatstr = 'account_levelup:uid=' + this.player.uid + ',username=' + this.getName() + ',level=' + base.level;
            formatlogger.info(formatstr);
            this.player.onLevelUpgrade();
        } else
            base.exp = maxExp;
    }

    var formatstr = 'account_addExp:uid=' + this.player.uid + ',username=' + this.getName() + ',exp=' + exp + ',expfinal=' + this.getExp();
    formatlogger.info(formatstr);

    var basePropSystem = this.player.getSystem("baseProp");
    basePropSystem.sendExpChangeMsg(this.getExp(), this.getLevel());

    this.player.setDBDirt(true);
}

handler.getExp = function () {
    var base = this.playerInfo.base;
    return base.exp;
}

handler.setExp = function (exp) {
    var base = this.playerInfo.base;
    base.exp = exp;

    this.player.setDBDirt(true);
}

handler.getMaxExp = function (level) {
    var tables = bearcat.getBean('tableMgr');
    var d = tables.exp.getEntry(level);
    if (!d)
        return -1;
    return d.exp;
}

handler.getMaxLevel = function () {
    var factory = bearcat.getBean('playerInfoFactory');
    return factory.commonConsts.MaxAccountLevel;
}

handler.setLevel = function (v) {
    var base = this.playerInfo.base;
    base.level = v;
    this.player.setDBDirt(true);
}

handler.getLevel = function () {
    var base = this.playerInfo.base;
    return base.level;
}

handler.getGold = function () {
    var base = this.playerInfo.base;
    return base.gold;
}

handler.addGold = function (off) {
    var base = this.playerInfo.base;
    var info = {};
    info.baseGold = base.gold;
    info.off = off;
    this.player.onGoldChange(info);
    if (off > 0) {
        base.gold += off;
    } else {
        if (base.gold >= off) {
            base.gold += off;
        } else {
            base.gold = 0;
        }
    }
    var formatstr = 'account_addGold:uid=' + this.player.uid + ',username=' + this.getName() + ',addgold=' + off + ',goldfinal=' + base.gold;
    formatlogger.info(formatstr);

    var factory = bearcat.getBean('playerInfoFactory');
    var basePropSystem = this.player.getSystem("baseProp");
    basePropSystem.sendPropChangeMsg(factory.commonConsts.BasePropType.Gold, off);

    this.player.setDBDirt(true);

    return base.gold;
}

handler.addDiamond = function (off) {
    var base = this.playerInfo.base;
    var opInfo = (arguments[1] ? arguments[1] : { type: '0', reason: '0' });
    var info = {};
    info.oldCount = base.diamond;
    info.cost = off;
    info.type = opInfo.type;
    info.reason = opInfo.reason;
    info.off = off;


    this.player.onDiamondChange(info);
    if (off > 0) {
        base.diamond += off;
    } else {
        if (base.diamond >= off) {
            base.diamond += off;
        } else {
            base.diamond = 0;
        }
    }

    info.nowCount = base.diamond;
    var formatLogAgent = bearcat.getBean('formatLogAgent');
    if (off > 0) {
        formatLogAgent.DiamondOutPut(this.player, info);
    } else {
        formatLogAgent.MinusDiamond(this.player, info);
    }

    var formatstr = 'account_addDiamond:uid=' + this.player.uid + ',username=' + this.getName() + ',adddiamond=' + off + ',diamondfinal=' + base.diamond;
    formatlogger.info(formatstr);

    var factory = bearcat.getBean('playerInfoFactory');
    var basePropSystem = this.player.getSystem("baseProp");
    basePropSystem.sendPropChangeMsg(factory.commonConsts.BasePropType.Diamond, off);

    this.player.setDBDirt(true);
    return base.diamond;
}

handler.getDiamond = function () {
    var base = this.playerInfo.base;
    return base.diamond;
}

module.exports = {
    id: "playerInfoAgent",
    func: PlayerInfoAgent,
    scope: "prototype",
    props: [{
        name: "utils",
        ref: "utils"
    }]
}