var logger = require('pomelo-logger').getLogger('2048', __filename, process.pid);

var SystemHandler = function(app) {
    this.app = app;
    this.channelService = app.get('channelService');
	this.stageMgrService = null;
	this.playerMgr = bearcat.getBean('playerMgr');
	this.consts = bearcat.getBean('consts');
};

var handler = SystemHandler.prototype;

handler.req = function(msg, session, next) {
	logger.info('req:', msg);
	var player = this.playerMgr.getPlayer(session.uid);
	if(player) {	
		player.onSystemReq(msg);	
	}
	next(null,{code:200});
}

handler.reqCb = function(msg, session, next) {
	logger.info('req:', msg);
	var player = this.playerMgr.getPlayer(session.uid);
	if(player) {	
		player.onSystemReqCb(msg,function(ret){
			next(null,ret);
		});	
	}
	else{
		next(null,null);
	}

}



module.exports = function(app) {	
	var ret = bearcat.getBean({
		id: 'sysH',
		func: SystemHandler,
		args: [{
			name: 'app',
			value: app
		}],
		props: [{
			name: 'stageMgrService',
			ref: 'stageMgrService'
		}]
	});	
	return ret;
};