var logger = require('pomelo-logger').getLogger('2048', __filename, process.pid);
var kakassilog = require('pomelo-logger').getLogger('kakassidebug');
var matchlog = require('pomelo-logger').getLogger('matchlog');

var G2048Handler = function(app) {
    this.app = app;
    this.channelService = app.get('channelService');
	this.stageMgrService = null;
	this.playerMgr = bearcat.getBean('playerMgr');
	this.consts = bearcat.getBean('consts');
};

var handler = G2048Handler.prototype;


handler.systemReq = function(msg, session, next) {	
	var player = this.playerMgr.getPlayer(session.uid);
	if(player) {
		player.onSystemReq(msg);		
	}
	next(null,{code:200});
}


module.exports = function(app) {	
	var ret = bearcat.getBean({
		id: 'g2048Handler',
		func: G2048Handler,
		args: [{
			name: 'app',
			value: app
		}],
		props: [{
			name: 'stageMgrService',
			ref: 'stageMgrService'
		}]
	});	
	return ret;
};