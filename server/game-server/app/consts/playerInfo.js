/*
var SysUtil = require('util');
var BaseData = require('./basedata');
// -----------------------------------------------------
// 基础信息
function SPlayerBaseInfo() {
    this.name = '';
    this.level = 1;     // 等级
    this.exp = 0;       // 经验值
    this.gold = 0;      // 金币
    this.diamond = 0;   // 钻石
}
SysUtil.inherits(SPlayerBaseInfo, BaseData);
var __proto = SPlayerBaseInfo.prototype;

__proto.loadFromJsonObj = function(jsonObj) {
    var LUtils = Game.Loader.LUtils;
    LUtils.valueLoadObj(jsonObj, this);
}

__proto.saveToJsonObj = function(jsonObj) {
    var LUtils = Game.Loader.LUtils;

    LUtils.valueSaveObj(this, jsonObj);
}



// -----------------------------------------------------

var SPlayerInfo = function() {
    this.base = new SPlayerBaseInfo();
}
SysUtil.inherits(SPlayerInfo, BaseData);
var __proto = SPlayerInfo.prototype;

__proto.loadFromJsonObj = function(jsonObj) {
    var LUtils = Game.Loader.LUtils;
    LUtils.loadChildObj(this.base, jsonObj, 'base');
}

__proto.saveToJsonObj = function(jsonObj) {
    var LUtils = Game.Loader.LUtils;
    LUtils.saveChildObj(this.base, jsonObj, 'base');
}

var PlayerInfoFactory = function() {
    this.SPlayerInfo = SPlayerInfo;
    this.LUtils = Game.Loader.LUtils;
}

module.exports = {
    id: "playerInfo",
    func: PlayerInfo
}
*/

