
var DBAuthInfo = function(){
    this.channel = null;
    this.user = '';
    this.pass = '';
    this.params = null;// params,客户端组织上来的详细信息
    this.channelUId = '';
}

var DBAuthRet = function() {
    this.code = 0;
    this.uid = -1;              // 唯一id,角色
    this.channel = '';          // 渠道名

    this.channelUId = '';        // 渠道uid
    this.errstr = '';           // 错误信息
    
    this.isGuest = false;       // 游客
    this.userSource = 0;
}

//connect 2 logic 这条消息倒确实不是dbmsg
var MsgPlayerEnter = function(){
    this.uid = 0;
    this.frontendId = 0;
}

//From Logic to DB
var DBReqLoadPlayer = function() {
    this.uid = -1;          //
}

//From DB to Logic
var DBAckLoadPlayer = function() {
    this.result = false;            //
    this.playerInfo = '';
}

var DBReqSavePlayer = function() {
    this.uid = -1;
    this.info = '';
    this.brief = null;
}

var DBAckSavePlayer = function() {    
    this.result = false;
}



//---------------------------------------------------------

var DBMsgDefine = function() {
    this.DBAuthInfo = DBAuthInfo;
    this.DBAuthRet = DBAuthRet;
    this.MsgPlayerEnter = MsgPlayerEnter;
    this.DBReqLoadPlayer = DBReqLoadPlayer;
    this.DBAckLoadPlayer = DBAckLoadPlayer;
    this.DBReqSavePlayer = DBReqSavePlayer;
    this.DBAckSavePlayer = DBAckSavePlayer;
}

module.exports = {
  id: "dBMsgDefine",
  func: DBMsgDefine
}
