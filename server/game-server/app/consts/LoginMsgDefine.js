
//申请登录
function MsgC2SReqLogin() {
    this.channel = '';
    this.username = '';      //    
    this.params = '';    //String  
}

function MsgS2CAckLogin() {
    this.code = 0;                          //    
    this.uid = 0;                           // 
    this.isOfflineReEnter = false;
    this.isGuest = false;                   // 游客
}

var LoginMsgFactory = function() {
    this.MsgC2SReqLogin = MsgC2SReqLogin;
    this.MsgS2CAckLogin = MsgS2CAckLogin;
}

module.exports = {
    id: "loginMsgFactory",
    func: LoginMsgFactory
}
