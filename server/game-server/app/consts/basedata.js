var SysUtil = require('util');

var LoaderUtils = function() {        
}
var __proto = LoaderUtils.prototype;

__proto.isValueObj = function(obj) {
    if(typeof(obj) == 'number' ||
        typeof(obj) == 'boolean'|| 
        typeof(obj) == 'string') {
        return true;
    }
    if(obj == null)
        return true;
    return false;
}

__proto._valueCopyObj = function(fromObj, obj, refObj) {
    var keys = Object.keys(refObj);    
    var key;        
    var fromValue;
    for(var i = 0 ; i < keys.length ; i++) {
        key = keys[i];            
        
        fromValue = fromObj[key];            

        if(typeof(fromValue) == 'number' ||
            typeof(fromValue) == 'boolean'|| 
            typeof(fromValue) == 'string') {
            obj[key] = fromValue;
            continue;
        }    
    }    
}    

// fromObj -> obj    
__proto.valueCopyObj = function(fromObj, obj) {
    this._valueCopyObj(fromObj, obj, fromObj);
}    

__proto.valueLoadObj = function(fromObj, obj) {
    this._valueCopyObj(fromObj, obj, obj);
}    

    __proto.valueSaveObj = function(fromObj, obj) {
    this._valueCopyObj(fromObj, obj, fromObj);
}    

__proto.loadInt = function(jsonObj, name, def) {
    var value = jsonObj[name];
    if(!value)
        return def;
    if(typeof(value) == 'number') {
        return Number(value);
    }        
    return def;
}

__proto.loadStr = function(jsonObj, name, def) {
    var value = jsonObj[name];
    if(!value)
        return def;
    if(typeof(value) == 'string') {
        return value;
    }        
    return def;
}

// 载入对象列表
__proto.loadArray = function(jsonObj, name, classObj) {
    var obj = jsonObj[name];
    if(!obj || !(obj instanceof Array))
        return [];
    var ret = [];
    for(var i = 0; i < obj.length; i ++) {
        var one = new classObj();
        one.loadFromJsonObj(obj[i]);
        ret.push(one);
    }
    return ret;
}

// 载入数值array
__proto.loadValueArray = function(jsonObj, name) {
    var obj = jsonObj[name];
    if(!obj || !(obj instanceof Array))
        return [];        
    var ret = [];
    for(var i = 0; i < obj.length; i ++) {            
        ret.push(obj[i]);
    }
    return ret;
}

__proto.saveArray = function(parent, name) {
    var ret = [];
    var obj = parent[name];
    if(!obj || !(obj instanceof Array))
        return ret;
    for(var i = 0; i < obj.length; i ++) {
        var one = obj[i];

        var item = null;
        if(one instanceof Array) {
            item = this.saveArray(obj, i);
        } 
        else {
            var item = one;
            if(typeof(one) == 'object') {
                item = {};
                one.saveToJsonObj(item);    
            }     
        }       
        ret.push(item);
    }
    return ret;
}

__proto.loadChildObj = function(obj, jsonObj, name) {
    var childJsonObj = jsonObj[name];
    if(!childJsonObj)
        return;
    obj.loadFromJsonObj(childJsonObj);
}

__proto.saveChildObj = function(obj, jsonObj, name) {
    jsonObj[name] = {};        
    obj.saveToJsonObj(jsonObj[name]);
}

__proto.loadMapObj = function(jsonObj, name, classObj) { 
    var childJsonObj = jsonObj[name];
    if(!childJsonObj)
        return {};
    var ret = {};
    var keys = Object.keys(childJsonObj);
    for(var i = 0; i < keys.length; i ++) {
        var from = childJsonObj[keys[i]];
        if(classObj) {
            var one = new classObj();
            one.loadFromJsonObj(from);    
            ret[keys[i]] = one;
        }
        else {                
            var one = null;
            if(this.isValueObj(from)) {
                one = from;
            }
            else {
                one = {};
                this.valueCopyObj(from, one);                    
            }
            
            ret[keys[i]] = one;
        }            
        
    }
    return ret;
}

__proto.saveMapObj = function(parent, name) {
    var ret = {};
    var obj = parent[name];
    if(!obj || typeof(obj) != 'object')
        return ret;
    var keys = Object.keys(obj);
    for(var i = 0; i < keys.length; i ++) {
        var one = obj[keys[i]];

        var oneobj = null;
        var hasFunc = SUtils.tryGetFunc(one, 'saveToJsonStr');
        if(!!hasFunc) {
            oneobj = {};
            one.saveToJsonObj(oneobj); 
        }
        else {
            if(this.isValueObj(one)) {
                oneobj = one;
            } 
            else {
                oneobj = {};
                this.valueCopyObj(one, oneobj);    
            }                
        }

        if(oneobj!=null)
            ret[keys[i]] = oneobj;
    }
    return ret;
}

__proto._saveToJsonStr = function(obj) {
    var jsonObj = {};
    obj.saveToJsonObj(jsonObj);
    return JSON.stringify(jsonObj);
}

__proto.saveToJsonStr = function(obj) {
    return this._saveToJsonStr(obj);
}

__proto.loadFromJsonStr = function(obj, content) {
    var jsonObj = JSON.parse(content);
    obj.loadFromJsonObj(jsonObj);
    return obj;
}

__proto.jsonStringify = function(obj) {
    try {
        return JSON.stringify(obj);    
    }
    catch(err) {
        return '';
    }
}

__proto.jsonParse = function(content) {
    try {
        return JSON.parse(content);
    }
    catch(err) {
        return {};            
    }
}

// ----------------------------------------------
var BaseData = function() {	
}
var __proto = BaseData.prototype;

__proto.loadFromJsonStr = function(content) {
    var LUtils = Game.Loader.LUtils;

    LUtils.loadFromJsonStr(this, content);
}

__proto.saveToJsonStr = function() {
    var LUtils = Game.Loader.LUtils;

    return LUtils.saveToJsonStr(this);
}

__proto.loadFromJsonObj = function(jsonObj) {	
    var LUtils = Game.Loader.LUtils;

    LUtils.valueLoadObj(jsonObj, this);
}    

__proto.saveToJsonObj = function(jsonObj) {	
    var LUtils = Game.Loader.LUtils;

    LUtils.valueSaveObj(this, jsonObj);
}

// ----------------------------------------------
var ExampleData1 = function() {
    BaseData.call(this);

    this.k = 0;
    this.b = 'ddd';        
}
SysUtil.inherits(ExampleData1, BaseData);
var __proto = ExampleData1.prototype;

__proto.loadFromJsonObj = function(jsonObj) {
    var lutils = Game.Loader.LUtils;
    
    lutils.valueLoadObj(jsonObj, this);       
}

__proto.saveToJsonObj = function(jsonObj) {
    var lutils = Game.Loader.LUtils;
    
    lutils.valueSaveObj(this, jsonObj);        
}

// 
var ExampleData = function() {
    BaseData.call(this);

    this.a = 0;
    this.b = 'ddd';
    this.c = [];
    this.d = {a:1,b:2};
    this.a1 = new ExampleData1();
    this.e = {a:new ExampleData1(),
        b:new ExampleData1()}
}
SysUtil.inherits(ExampleData, BaseData);
var __proto = ExampleData.prototype;

__proto.loadFromJsonObj = function(jsonObj) {
    var lutils = Game.Loader.LUtils;
    
    lutils.valueLoadObj(jsonObj, this);
    this.c = lutils.loadValueArray(jsonObj, 'c');
    this.d = lutils.loadMapObj(jsonObj, 'd', null);
    lutils.loadChildObj(this.a1, jsonObj, 'a1');
    this.e = lutils.loadMapObj(jsonObj, 'e', ExampleData1);
}

__proto.saveToJsonObj = function(jsonObj) {
    var lutils = Game.Loader.LUtils;
    
    lutils.valueSaveObj(this, jsonObj);
    jsonObj.c = lutils.saveArray(this, 'c');
    jsonObj.d = lutils.saveMapObj(this, 'd');
    lutils.saveChildObj(this.a1, jsonObj, 'a1');
    jsonObj.e = lutils.saveMapObj(this, 'e');
}

// ---------------------------------------------    

//Game.Loader.BaseData = BaseData;
//Game.Loader.ExampleData = ExampleData;
//Game.Loader.LUtils = new LoaderUtils();    

